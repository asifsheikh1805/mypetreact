import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import './css/index.css';
import registerServiceWorker from './registerServiceWorker';
import store from "./js/_store/index";
import App from './js/App';

ReactDOM.render(
	<Provider store={store}>
    	<App />
  	</Provider>, document.getElementById('root'));
registerServiceWorker();


window.store = store;