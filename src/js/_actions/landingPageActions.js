import { alertActions } from './';
import { landingPageConstants } from '../_constants/action-types';
import { landingPageServices } from '../_services';

export const showMobileViewIndex = showMobileLoginForm => ({ 
	type: landingPageConstants.SHOW_MOBILE_VIEW_INDEX, 
	showMobileLoginForm: showMobileLoginForm
});

export function registerUser(user) {
	const success = (user) => { return {type: landingPageConstants.REGISTER_SUCCESS, user}};
	const register = (user) => { return {type: landingPageConstants.REGISTER_REQUEST, user}};
	const failure = (error) => { return {type: landingPageConstants.REGISTER_FAILURE, error}};

	return dispatch => {
		dispatch(register(user));

		landingPageServices.register(user)
		.then(
			data => {
				if(data.isSuccess){
					dispatch(success(data));
					dispatch(alertActions.success("Registration Successful"));
				} else {
					dispatch(failure(data));
					dispatch(alertActions.error(data.errMsg));
				}
			},
			error => {
				dispatch(failure(error));
				dispatch(alertActions.error(error.message));
			}
		);
	};

}