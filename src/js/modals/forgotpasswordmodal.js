import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import '../../css/main.css';
import '../../css/layout.css';

class Forgotpassword extends React.Component{
	render() {
		return (
			<Modal {...this.props} aria-labelledby="contained-modal-title-sm">
				<Modal.Header closeButton>
					<Modal.Title>Forgot Password</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<form id="forgot-password-form" name="forgot-password" method="POST" novalidate>
						<div>
				      		<input type="text" className="form-control" id="forgotpasswordemail" name="forgotpasswordemail" placeholder="Email or Mobile" required />
				      	</div>
				      	<div className="text-center">
				      		<Button id="forgotpassword" bsStyle="success" type="submit">Recover</Button>
				      	</div>
					</form>
				</Modal.Body>
			</Modal>
		);
	}
}

export default Forgotpassword;