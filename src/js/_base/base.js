export const commonFunctions = {
    detectmobile,
    validatenumberoremail,
};

function detectmobile() { 
	 if( navigator.userAgent.match(/Android/i)
	 || navigator.userAgent.match(/webOS/i)
	 || navigator.userAgent.match(/iPhone/i)
	 || navigator.userAgent.match(/iPad/i)
	 || navigator.userAgent.match(/iPod/i)
	 || navigator.userAgent.match(/BlackBerry/i)
	 || navigator.userAgent.match(/Windows Phone/i)
	 ){
	    return true;
	  }
	 else {
	    return false;
	  }
}

function validatenumberoremail(value){
	var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var number = /^\d{3}-?\d{3}-?\d{4}$/g;
    
    if(value){
    	if (email.test(value) || number.test(value))
	    	return 'success';
	    else 
	    	return 'error';
    }

    return null;
}