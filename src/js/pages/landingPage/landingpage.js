import React from 'react';
import { connect } from "react-redux";
import LoginForm from './loginForm.js';
import SignupForm from './signupForm.js';
import { commonFunctions } from '../../_base';
import { showMobileViewIndex } from "../../_actions";

const mapStateToProps = state => {
  return { showMobileLoginForm: state.landingpage.showMobileLoginForm };
};

const mapDispatchToProps = dispatch => ({
  toggleMobileView: (payload) => dispatch(showMobileViewIndex(payload))
});

class LandingPage extends React.Component {

	render() {
		return (
		  	<div className="main" role="main">
				<section className="video-holder">
				  <div className="container">
				    <div className="videoflex">
				      <figure>
				        <YoutubeIframe video="nHFIHq0eTbo" autoplay="0" rel="0" modest="1" />
				      </figure>
				      <section>
				        <div className="contact-form-home">
				          <h4>Create New Account</h4>
				          <div className="cont-form-home">
				          	<SignupForm />
				          	{commonFunctions.detectmobile() ? <LoginForm /> : null}

				          	<div className="signup" style={(!this.props.showMobileLoginForm && commonFunctions.detectmobile()) ? { display: 'block' } : { display: 'none' }}>
				              <span>Not a member? 
				                <a href="javascript:;" id="joinnow" onClick={() => this.props.toggleMobileView(!this.props.showMobileLoginForm)}>Join Now</a>
				              </span>
				            </div>

				            <div className="signin" style={this.props.showMobileLoginForm ? { display: 'block' } : { display: 'none' }}>
				              <span>Already have an account? 
				                <a href="javascript:;" id="signin" onClick={() => this.props.toggleMobileView(!this.props.showMobileLoginForm)}>Sign In</a>
				              </span>
				            </div>

				            <div className="or">
				              <span>Or</span>
				            </div>
				            <div className="share-home">
				              	<div id="facebook-login"><div><i className="fa fa-facebook"></i></div><span>Sign In</span></div>
								<div className="g-signin2" data-onsuccess="onGoogleSignIn" data-onload="false"></div>
				            </div>
				          </div>
				        </div>
				      </section>
				    </div>
				  </div>
				</section>
			</div>
		);
	}
}

class YoutubeIframe extends React.Component {
	render() {
		var videoSrc = "https://www.youtube.com/embed/" + 
        this.props.video + "?autoplay=" + 
        this.props.autoplay + "&rel=" + 
        this.props.rel + "&modestbranding=" +
        this.props.modest;
	    return (
	        <iframe className="player" type="text/html" width="100%" height="100%" src={videoSrc} frameBorder="0" allowFullScreen/>
	    );
	}	
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);