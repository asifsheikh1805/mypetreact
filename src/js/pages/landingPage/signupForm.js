import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from "react-redux";
import { commonFunctions } from '../../_base';
import { registerUser } from '../../_actions';
import { FieldGroup, ShowTooltip } from '../../components/common';
import Button from 'react-bootstrap/lib/Button';
import FontAwesome from 'react-fontawesome';

const mapStateToProps = state => {
  return { showMobileLoginForm: !state.landingpage.showMobileLoginForm,
            registering: state.registration.registering
          };
};

class SignupForm extends React.Component {
	constructor(props, context) {
    super(props, context);

    this.state = {
        user: {
            fullname: '',
            email: '',
            password: ''
        },
        submitted: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;

        if(user.fullname && user.email && user.password){
          dispatch(registerUser(user));
        }
    }

  render() {
  	const { user, submitted } = this.state;
    const registering = this.props.registering;
    return (
      <form onSubmit={this.handleSubmit} style={this.props.showMobileLoginForm ? { display: 'block' } : { display: 'none' }}>
        <FieldGroup
    			id="fullname"
    			type="text"
    			validationState={null}
    			value={user.fullname}
    			placeholder="Full Name"
    			onChange={this.handleChange}
    			name= "fullname"/>

    		<FieldGroup
    			id="email"
    			type="email"
    			validationState={commonFunctions.validatenumberoremail(this.state.user.email)}
    			value={user.email}
    			placeholder="Email Address or Mobile"
    			onChange={this.handleChange}
    			name= "email"/>

    		<FieldGroup
    			id="password"
    			type="password"
    			validationState={null}
    			value={user.password}
    			placeholder="Password"
    			onChange={this.handleChange}
    			name= "password"/>

		
			<Button bsStyle="primary" id="signup" type="submit" block onClick={this.handleToggle}><FontAwesome
        name='rocket'
        size='2x'
        spin
        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}/></Button>
      {registering && 
      <FontAwesome
        className='super-crazy-colors'
        name='rocket'
        size='2x'
        spin
        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}/>
      }
        <small className="form-text">By clicking Join Now, you agree to the Our User Agreement, Privacy Policy, and Cookie Policy.</small>
      </form>
    );
  }
	/*render() {
		return(
			<form id="signin-form" name="registration" method="POST"  style={this.props.showMobileLoginForm ? { display: 'block' } : { display: 'none' }}>
                <div className="input">
                  <input type="text" className="form-control" name="fullname" id="fullname" placeholder="Full Name" required />
                </div>
                <div className="input">
                  <input type="text" className="form-control" name="email" id="email" placeholder="Email Address or Mobile" required />
                </div>
                <div className="input">
                  <input type="password" className="form-control" name="password" id="password" placeholder="Password" required />
                </div>
              <Button bsStyle="primary" id="signup" type="submit" block>Join Now</Button>
              <small className="form-text">By clicking Join Now, you agree to the Our User Agreement, Privacy Policy, and Cookie Policy.</small>
            </form>
		);
	}
*/}

export default connect(mapStateToProps, null)(SignupForm);