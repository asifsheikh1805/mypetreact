import React from 'react';
import { connect } from "react-redux";
import Forgotpasswordmodal from '../../modals/forgotpasswordmodal.js';
import { commonFunctions } from '../../_base';
import Button from 'react-bootstrap/lib/Button';

const mapStateToProps = state => {
  return { showMobileLoginForm: state.landingpage.showMobileLoginForm };
};

class LoginForm extends React.Component{
	render() {
		return(
			<div>
				{commonFunctions.detectmobile() ? <MobileLoginForm show={this.props.showMobileLoginForm}/> : <DesktopLoginForm /> }
			</div>
		);
	}
}

class DesktopLoginForm extends React.Component{
	constructor(props, context) {
	    super(props, context);

	    this.state = {
	      showForgotPasswordModal: false
	    };
	}

	render() {
		let forgotPasswordModalClose = () => this.setState({ showForgotPasswordModal: false });

		return (
			<div>
				<div className="header-top-right row">
					<span className="register">
						<form id="login-form" name="login" method="POST" novalidate>
							<div className="col-md-4">
								<div>
									<input type="text" className="form-control" id="loginemail" name="loginemail" placeholder="Email or Mobile" required />
								</div>
							</div>
					        <div className="col-md-4">
					        	<div>
						      		<input type="password" className="form-control" id="loginpassword" name="loginpassword" placeholder="Password" required />
						      	</div>
						      	<div className="forgotPassword">
									<a href="javascript:;" onClick={() => this.setState({showForgotPasswordModal: true})}>Forgot Password</a>
								</div>
						    </div>
						    <div className="col-md-2">
								<Button id="login" bsStyle="warning" type="submit">Sign In</Button>
							</div>
					    </form>
					</span>
				</div>
				<Forgotpasswordmodal show={this.state.showForgotPasswordModal} onHide={forgotPasswordModalClose} /> 
			</div>
		);
	}
}

class MobileLoginForm extends React.Component {
	constructor(props, context) {
	    super(props, context);

	    this.state = {
	      showForgotPasswordModal: false,
	    };
	}

	render() {
		let forgotPasswordModalClose = () => this.setState({ showForgotPasswordModal: false });

		return(
			<div style={this.props.show ? { display: 'block' } : { display: 'none' }}>
				<form id="mobile-login-form" name="mobilelogin" method="POST" style={this.props.show ? { display: 'block' } : { display: 'none' }}>
					<div>
						<div className="input">
						  <input type="text" className="form-control" id="loginemail" name="loginemail" placeholder="Email or Mobile" required />
						</div>
					</div>
					<div>
					    <div className="input">
					      <input type="password" className="form-control" id="loginpassword" name="loginpassword" placeholder="Password" required />
					    </div>
					    <div className="forgotPassword">
					      <a href="javascript:;" onClick={() => this.setState({showForgotPasswordModal: true})}>Forgot Password</a>
					    </div>
					</div>
					<div>
						<Button id="mobilelogin" bsStyle="warning" type="submit" block>Sign In</Button>
					</div>		
				</form>
				<Forgotpasswordmodal show={this.state.showForgotPasswordModal} onHide={forgotPasswordModalClose} />
			</div>
		);
	}
}

export default connect(mapStateToProps, null)(LoginForm);