import React from 'react'; 
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Tooltip from 'react-bootstrap/lib/Tooltip';


export function FieldGroup({ id, label, validationState, help, ...props }) {
  return (
    <FormGroup controlId={id} validationState = {validationState} >
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      <FormControl.Feedback />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export function ShowTooltip({ id, children, href, tooltip, placement="top", className='tooltip-danger', show='false' }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip className={className} id={id}>{tooltip}</Tooltip>}
      placement= {placement}
      delayShow={0}
      delayHide={150}
      defaultOverlayShown
    >
      <a href={href}>{children}</a>
    </OverlayTrigger>
  );
}