import React from 'react';
import logo from '../../images/logo-home.png';
import { commonFunctions } from '../_base';
import LoginForm from '../pages/landingPage/loginForm.js';
import '../../css/main.css';
import '../../css/layout.css';
import '../../css/landing-page.css';

class Header extends React.Component{
	render() {

		return (
			<div>
				<header>
					<div className="header-top">
						<div className="container">
							<div className="header-top-left">
								<div className="logo">
									{/*
										<img src={logo} />
									*/}
								</div>
							</div>
							{!commonFunctions.detectmobile() ? <LoginForm /> : null}
						</div>
					</div>	
				</header>
			</div>
		);
	}
}

export default Header;
