import React from 'react';

class Footer extends React.Component {
  render() {
    return (
      	<footer className="footer">
			<div className="container">
				<div className="links">
					<a href="#" className="link">ABOUT US</a>     
					<a href="#" className="link">SUPPORT</a>     
					<a href="#" className="link">BLOG</a>     
					<a href="#" className="link">PRESS</a>     
					<a href="#" className="link">API</a>     
					<a href="#" className="link">JOBS</a>     
					<a href="#" className="link">PRIVACY</a>     
					<a href="#" className="link">TERMS</a> 
				</div>
				<p className="copyright">© 2017 My Pet Universe</p>
				<div className="clear"></div>
			</div>
		</footer>
    );
  }
}

export default Footer;