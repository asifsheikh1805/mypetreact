export const alertConstants = {
	/*Alert Constants*/
	SUCCESS: 'ALERT_SUCCESS',
   	ERROR: 'ALERT_ERROR',
    CLEAR: 'ALERT_CLEAR',
	}


export const landingPageConstants = {
    /*Signup Page Constants*/
	SHOW_MOBILE_VIEW_INDEX: "SHOW_MOBILE_VIEW_INDEX",

	REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',
	}
