import { combineReducers } from 'redux';

import { landingpage, registration } from './landingPageReducers';
import { alert } from './alertReducers';

const rootReducer = combineReducers({
  landingpage,
  registration,
  alert
});

export default rootReducer;