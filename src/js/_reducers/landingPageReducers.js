import { landingPageConstants } from "../_constants/action-types";
const initialState = {
  showMobileLoginForm: false,
};

export function landingpage(state = initialState, action){
  switch (action.type) {
    case landingPageConstants.SHOW_MOBILE_VIEW_INDEX:
      return { ...state, showMobileLoginForm: action.showMobileLoginForm};
    default:
      return state;
  }
}

export function registration(state = {}, action){
  switch (action.type) {
    case landingPageConstants.REGISTER_REQUEST:
      return { registering: true };
    case landingPageConstants.REGISTER_SUCCESS:
      return {};
    case landingPageConstants.REGISTER_FAILURE:
      return { /*registering: false */};
    default:
      return state;
  }
}