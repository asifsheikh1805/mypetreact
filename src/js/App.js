import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import LandingPage from './pages/landingPage/landingpage';
import { alertActions } from './_actions';
import Alert from 'react-bootstrap/lib/Alert';
import Row from 'react-bootstrap/lib/Row';

const mapStateToProps = (state) => ({alert: state.alert});

const mapDispatchToProps = (dispatch) => ({
  dismissAlert: () => dispatch(alertActions.clear())
});
  
class App extends Component {
  constructor(props) {
    super(props);
  }

  /*fetchData(url) {
     let requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    };
        this.setState({ isLoading: true });
        fetch(url, requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                    console.log(response);
                    return Promise.reject(response.statusText);
                }
                this.setState({ isLoading: false });
                console.log(response.json());
                return response.json();
            }).then((data) =>{
                console.log(data);
            });
    }
    componentWillMount() {
        this.fetchData('http://localhost:8095/global/publicshorturl?url=https://www.google.com');
    }*/

  componentDidMount() {
    setTimeout(() => {
     this.props.dismissAlert();
    }, 3000);
  }

  render() {
    const { alert } = this.props;
    const { dismissAlert } = this.props;

    return (
      <div>
      {alert.message &&
        <div className="alert-container container">
          <Row className="show-grid">
            <Alert bsStyle={`${alert.type}`} onDismiss={dismissAlert}>
              {alert.message}
            </Alert>
          </Row>
        </div>
      }
        <div className="App">
          <Header />
          <LandingPage />
          <Footer />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
