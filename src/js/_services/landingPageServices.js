export const landingPageServices = {
	register
}

function handleResponse(response) {
    if (!response.ok) { 
        return Promise.reject(response.statusText);
    }

    return response.json();
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(user)
    };

    return fetch('http://localhost/mypetv2/my//application/action/IndexAction.php', requestOptions).then(handleResponse);
}

