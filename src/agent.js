import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost/react_project/firstproject/build/public/api';

const responseBody = res => res.body;

const requests = {
	get: url => superagent.get(`${API_ROOT}${url}`).then(responseBody)
};

const Articles = {
	all: page => requests.get('/test.php')
};

export default {
	Articles
};